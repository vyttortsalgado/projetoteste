package contaCliente.models;

public class Automovel {
    private Long distanciaPercorrida;
    private Long totalCombustivelGasto;

    public Automovel() {
        LeituraDadosConsole console = new LeituraDadosConsole();

        Long valor = Long.parseLong( console.lerDados("Digite o valor de distancia:") );
        this.distanciaPercorrida = valor;

        Long valorCombustivel = Long.parseLong( console.lerDados("Digite o valor de combustivel gasto:") );
        this.totalCombustivelGasto = valorCombustivel;
    }

    public Long getDistanciaPercorrida() {
        return distanciaPercorrida;
    }

    public void setDistanciaPercorrida(Long distanciaPercorrida) {
        this.distanciaPercorrida = distanciaPercorrida;
    }

    public Long getTotalCombustivelGasto() {
        return totalCombustivelGasto;
    }

    public void setTotalCombustivelGasto(Long totalCombustivelGasto) {
        this.totalCombustivelGasto = totalCombustivelGasto;
    }

    public Double calcularConsumoMedio(){
        return 10D;
    }
}
