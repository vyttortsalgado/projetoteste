package contaCliente.models;

import java.util.Scanner;

public class LeituraDadosConsole {
    public String lerDados(String msg){
        Scanner scanner = new Scanner(System.in);

        System.out.println(msg);
        String valorLido = scanner.next();

//        scanner.close();

        return valorLido;
    }
}
