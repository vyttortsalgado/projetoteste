package contaCliente;

import contaCliente.models.Automovel;
import contaCliente.models.LeituraDadosConsole;

import javax.swing.*;
import java.util.ArrayList;

public class BankApplication {
    public static void main(String[] args) {
        Automovel automovel = new Automovel();

        System.out.println(automovel.calcularConsumoMedio());
    }
}
